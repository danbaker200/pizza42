Pizza42
=======

### App URL
https://pizza42-209502.appspot.com/

### Instructions
Use the login button in the top left hand corner and follow the instructions to login.

You will not be able to order Pizza until your email is verified.

User details (such as gender, google connection count) is available via the auth0 dashboard.



const express = require('express');
const next = require('next');
const auth0 = require('auth0');
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const fetch = require('isomorphic-unfetch');
const config = require('./config');

const port = parseInt(process.env.PORT, 10) || 8080;
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();

const auth0Client = new auth0.ManagementClient({
    domain: config.auth0Domain,
    clientId: config.auth0APIClientID,
    clientSecret: config.auth0APIClientSecret,
    scope: "read:users",
    audience: 'https://pizza42.au.auth0.com/api/v2/',
    tokenProvider: {
        enableCache: true,
        cacheTTLInSeconds: 10
    }
});

const verifyJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://pizza42.au.auth0.com/.well-known/jwks.json`
    }),

    audience: 'N-q7SvrM1BLKvyw1I8TeSZ3o33kAfxD3',
    issuer: `https://pizza42.au.auth0.com/`,
    algorithms: ['RS256'],
});

app.prepare()
    .then(() => {
        const server = express();

        // Update google connections count.
        // It would be nice if this was done in an auth0 rule but couldn't get that to work
        server.post('/api/login', verifyJwt, (req, res) => {
            auth0Client.users.get(req.user.sub, (err, resp) => {
                if (err || resp.length === 0) {
                    console.log(req.user.email, err, resp);
                    return;
                }
                const googProvider = resp[0].identities.find(i => i.provider === 'google-oauth2');
                if (!googProvider) {
                    console.log(req.user.email, "no goog provider found");
                    return;
                }

                fetch('https://content-people.googleapis.com/v1/people/me/connections?personFields=emailAddresses&pageSize=2000', {
                    headers: {
                        "Authorization": `Bearer ${googProvider.access_token}`
                    },
                }).then(resp => {
                    resp.json().then(data => {
                        if (data && data.connections) {
                            auth0Client.users.update({ id: req.user.sub }, {
                                user_metadata: {
                                    google_connections: data.connections.length,
                                }
                            }, (err) => console.log(req.user.email, err));
                        }
                    })
                })
            });
            res.send("OK");
        });

        server.post('/api/order', verifyJwt, (req, res) => {
            if (req.user.email_verified) {
                res.send('OK');
            } else {
                res.status(403).send('Unauthorized');
            }
        });

        server.get('*', (req, res) => {
            return handle(req, res);
        });

        server.listen(port, (err) => {
            if (err) throw err;
            console.log(`> Ready on http://localhost:${port}`)
        })
    });
import React from "react"
import Header from "../components/Header"
import fetch from "isomorphic-unfetch";

class MyApp extends React.Component {

    state = {
        loggedIn: false,
        emailVerified: false,
        msg: null,
    };

    componentDidMount() {
        const authState = window.sessionStorage.getItem("authState");
        if (authState) {
            this.setState(JSON.parse(authState));
        }
    }

    onLogin = (data) => {
        const authState = {
            loggedIn: !!data.idToken,
            emailVerified: data.idTokenPayload.email_verified,
            idToken: data.idToken,
            idTokenPayload: data.idTokenPayload,
            msg: null,
        };
        this.setState(authState);
        window.sessionStorage.setItem("authState", JSON.stringify(authState))
    };

    onLogout = () => {
        window.sessionStorage.removeItem("authState");
    };

    orderPizza = () => {
        fetch('/api/order', {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${this.state.idToken}`,
            },
        }).then(resp => {
            if (resp.ok) {
                this.setState({
                    msg: "Pizza successfully ordered!",
                });
            } else {
                this.onLogout();
                this.setState({
                    msg: "Pizza order unsuccessful. Please try logging in again",
                });
            }
        })
    };

    render() {
        return (
            <div>
                <Header
                    onLogin={this.onLogin}
                    onLogout={this.onLogout}
                    loggedIn={this.state.loggedIn}
                />
                <div className="container">
                    <img className="hero-image" src="/static/pizza.jpg"/>
                </div>
                <div className="container pizza-action">
                    {!this.state.loggedIn && (
                        <div>Please login to order some delicious pizza</div>
                    )}
                    {this.state.loggedIn && !this.state.emailVerified && (
                        <div>Please verify your email address to order some delicious pizza</div>
                    )}
                    {this.state.loggedIn && this.state.emailVerified && (
                        <div onClick={this.orderPizza} className="btn btn-primary">Order Pizza here</div>
                    )}
                    {this.state.msg && (
                        <p className="alert alert-info">{this.state.msg}</p>
                    )}
                </div>
                <style jsx>{`
                    .hero-image {
                        width: 100vw;
                        position: relative;
                        left: 50%;
                        right: 50%;
                        margin-left: -50vw;
                        margin-right: -50vw;
                    }

                    .pizza-action {
                         text-align: center;
                         margin-top: 30px;
                    }

                    p {
                        margin-top: 20px;
                    }
                `}</style>
            </div>
        );
    }
}

export default MyApp;

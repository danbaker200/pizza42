import React from "react"
import Header from "../components/Header"

export default class UserPage extends React.Component {

    state = {
        loggedIn: false,
        emailVerified: false,
        msg: null,
        idToken: null,
        idTokenPayload: null,
    };

    onLogin = (data) => {
        const authState = {
            loggedIn: !!data.idToken,
            emailVerified: data.idTokenPayload.email_verified,
            idToken: data.idToken,
            idTokenPayload: data.idTokenPayload,
            msg: null,
        };
        this.setState(authState);
        window.sessionStorage.setItem("authState", JSON.stringify(authState))
    };

    onLogout = () => {
        window.sessionStorage.removeItem("authState");
    };

    componentDidMount() {
        const authState = window.sessionStorage.getItem("authState");
        if (authState) {
            this.setState(JSON.parse(authState));
        }
    }

    render() {
        const userKeys = {
            "gender": "Gender",
            "name": "Name",
            "email": "Email",
            "email_verified": "Email verified",
            "sub": "ID",
        };

        return (
            <div>
                <Header
                    onLogin={this.onLogin}
                    onLogout={this.onLogout}
                    loggedIn={this.state.loggedIn}
                />
                <div className="container">
                    {!this.state.loggedIn && (
                        <div>Please log in to see user data</div>
                    )}
                    {this.state.loggedIn && (
                        <ul className="list-group">
                            {Object.keys(this.state.idTokenPayload).sort().reverse().map(key => {
                                if (userKeys[key]) {
                                    return <li className="list-group-item" key={key}>{userKeys[key]}: {"" + this.state.idTokenPayload[key]}</li>
                                }
                            })}
                        </ul>
                    )}
                </div>
            </div>
        )
    }
}

import React from "react";
import PropTypes from "prop-types";
import Head from 'next/head';
import Link from 'next/link';
import Auth0Lock from 'auth0-lock';
import fetch from "isomorphic-unfetch";
import config from "../config";

export default class LoginPage extends React.Component {

    static propTypes = {
        onLogin: PropTypes.func.isRequired,
        onLogout: PropTypes.func.isRequired,
        loggedIn: PropTypes.bool.isRequired,
    };

    componentDidMount() {
        this.lock = new Auth0Lock(config.auth0WebClientID, config.auth0Domain, {
            autoclose: true,
            auth: {
                redirectUrl: config.appURL,
                responseType: 'token id_token',
                audience: `pizza42`,
                params: {
                    scope: 'openid profile email user_metadata'
                }
            }
        });

        this.lock.on('authenticated', (result) => {
            fetch('/api/login', {
                method: "POST",
                headers: {
                    "Authorization": `Bearer ${result.idToken}`,
                },
            });
            this.props.onLogin(result)
        });


        this.lock.on('authorization_error', (err) => {
            alert(`Login error occured: ${err.toString()}`);
        });

    }

    login = () => {
        this.lock.show();
    };

    logout = () => {
        this.props.onLogout();
        this.lock.logout({
            returnTo: config.appURL,
        });
    };


    render() {
        return (
            <div>
                <Head>
                    <title>Pizza 42</title>
                    <link href="/static/bootstrap.css" rel="stylesheet"/>
                </Head>
                <nav className="navbar justify-content-between">
                    <div className="navbar-brand">
                        <Link href="/">
                            Pizza42
                        </Link>
                    </div>
                    <div>
                        {!this.props.loggedIn && (
                            <button type="button" className="btn btn-primary" onClick={this.login}>
                                Login
                            </button>
                        )}
                        {this.props.loggedIn && (
                            <div>
                                <Link href="/user">
                                    <button type="button" className="btn btn-secondary">
                                        View user details
                                    </button>
                                </Link>
                                <button type="button" className="btn btn-primary" onClick={this.logout}>
                                    Log out
                                </button>
                            </div>
                        )}
                    </div>
                </nav>
                <style jsx>{`
                    .btn-secondary {
                        margin-right: 10px;
                    }
                `}</style>
            </div>
        );
    }
}
